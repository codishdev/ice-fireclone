<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function() {
    Route::get('/external-books/{name}', 'Books\BookController@external_books')->name('book.api');
    Route::get('/books', 'Books\BookController@books')->name('book.list');
    Route::post('/books', 'Books\BookController@store')->name('book.store');
    Route::get('/books/{searchParam}', 'Books\BookController@show')->name('book.show');
    Route::patch('/books/{id}', 'Books\BookController@update')->name('book.update');
    Route::delete('/books/{id}', 'Books\BookController@delete')->name('book.delete');
});
