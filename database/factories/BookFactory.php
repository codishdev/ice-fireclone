<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'isbn' => $faker->sentence,
        'country' => $faker->country,
        'publisher' => $faker->name,
        'number_of_pages' => mt_rand(000, 100),
        'release_date' => $faker->date,
    ];
});
