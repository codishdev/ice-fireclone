<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Book;
use App\Models\Author;

class BookTest extends TestCase
{


    /**
     * A basic unit test example.
     *
     * @return void
     * @test
     */
    public function can_create_book()
    {
        $book = factory(Book::class)->create();
        $data = [
            'name' => $this->faker->name,
            'isbn' => $this->faker->sentence,
            'authors' => $this->faker->name,
            'country' => $this->faker->country,
            'publisher' => $this->faker->name,
            'number_of_pages' => mt_rand(000, 100),
            'release_date' => $this->faker->date,
        ];

        $author = new Author([
            'name' => $this->faker->name,
            'book_id' => $book->id,
        ]);

        $book->authors()->save($author);
    
       $response = $this->post('api/v1/books', $data);
       $response->assertStatus(201);
       $response->assertJsonStructure(['status_code','status','data']);
    }

    /**
     * @test
     */
    public function can_update_book() {
        $book = factory(Book::class)->create();
        $found = Book::find($book->id);
        $data = array(
            'name' => $this->faker->name,
            'isbn' => $this->faker->name,
            'country' => $this->faker->country,
            'publisher' => $this->faker->name,
            'number_of_pages' => mt_rand(000, 100),
            'release_date' => $this->faker->date,     
        );

       $response = $this->patch(route('book.update', $found->id), $data);

       $response->assertStatus(200);
       $response->assertJsonStructure(['status_code', 'status', 'data']);
    }

    /** 
     * @test
     */
    public function can_show_book() {

        $book = factory(Book::class)->create();

        $this->get(route('book.show', $book->id))->assertStatus(200)
            ->assertJsonStructure(['status_code', 'status', 'data']);
    }

     /**
      * @test
      */
    public function can_delete_book() {

        $book = factory(Book::class)->create();

        $this->delete(route('book.delete', $book->id))
            ->assertJsonStructure(['status_code', 'status', 'data']);
    }

     /**
     * @test
     */
    // public function books() {
    //     $books = factory(Book::class, 2)->create()->map(function ($book) {
    //         return $book->only([
    //             'id', 
    //             'name', 
    //             'isbn', 
    //             'authors', 
    //             'publisher', 
    //             'country',
    //             'release_date', 
    //             'number_of_pages'
    //         ]);
    //     });

    //    $response = $this->get(route('book.list'));
    //    $response->assertStatus(200);
    //    $response->assertJson(['status_code' => 200, 'status' => 'success', 'data' => $books]);
    //         $response->assertJsonStructure(
    //              [ 
    //                 'id', 
    //                 'name', 
    //                 'isbn', 
    //                 'authors', 
    //                 'publisher', 
    //                 'country',
    //                 'release_date', 
    //                 'number_of_pages'
    //             ],
    //         );
    // }
    
}
