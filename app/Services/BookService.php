<?php
  namespace App\Services;

  use App\Repositories\BookRepository;

  class BookService {
    
    protected $bookRepo;

    public function __construct(BookRepository $bookRepo) {
      $this->bookRepo = $bookRepo;
    }

    public function create($request) {
      return $this->bookRepo->create($request);
    }

    public function show($id) {
      return $this->bookRepo->show($id);
    }

    public function update($id, $request) {
      return $this->bookRepo->update($id, $request->all());
    }

    public function delete($id) {
      return $this->bookRepo->delete($id);
    }

  }