<?php

namespace App\Http\Requests;

// use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'isbn' => 'string|required',
            'authors' => 'string|required',
            'publisher' => 'string|required',
            'release_date' => 'date|required',
            'country' => 'string|required',
            'number_of_pages' => 'integer'
        ];
    }

    public function messages() {
        return [
            'name.string' => 'Book name must a string ',
            'isbn.string' => 'Book ISBN bust be a string',
            'authors.string' => 'Author"s name must be a string',
            'publisher.string' => 'Publisher name must be a string',
            'release_date.date' => 'Release date must be a date',
            'country.string' => 'Country must be a tsring',
            'number_of_pages.string' => 'Number of pages must be a string',

            'name.required' => 'Book name is required ',
            'isbn.required' => 'Book ISBN is required',
            'authors.required' => 'Author"s name is required',
            'publisher.required' => 'Publisher name is required',
            'release_date.required' => 'Release date is required',
            'country.required' => 'Country is required',
            'number_of_pages.required' => 'Number of pages is required'
        ];
    }
}
