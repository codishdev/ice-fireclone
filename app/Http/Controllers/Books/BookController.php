<?php

namespace App\Http\Controllers\Books;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Request;
use App\Services\BookService;
use App\Repositories\BookRepository;

class BookController extends Controller
{
	protected $bookService;
	protected $bookRepo;
	
	public function __construct(BookService $bookService, BookRepository $bookRepo) {
		$this->bookService = $bookService;
		$this->bookRepo = $bookRepo;
	}

	public function external_books($book) {
		return $this->bookRepo->external_books($book);
	}

	public function books() {
		return $this->bookRepo->index();
	}

	public function show($id) {
		return $this->bookService->show($id);
	}

	public function store(BookRequest $request) {
		return $this->bookService->create($request);
	}

	public function update($id, Request $request) {
		return $this->bookService->update($id, $request);
	}

	public function delete($id) {
		return $this->bookService->delete($id);
	}
}
