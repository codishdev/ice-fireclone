<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $fillable = ['name', 'isbn', 'country', 'publisher', 'release_date', 'number_of_pages'];

    public function authors() {
        return $this->hasMany(Author::class);
    }
}
