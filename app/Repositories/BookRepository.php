<?php 
  namespace App\Repositories;

  use App\Models\Book;
  use App\Models\Author;
  use App\Http\Resources\BookResource;
  use GuzzleHttp\Client;
  
  class BookRepository {
    protected $book;

    public function __construct(Book $book) {
      $this->book = $book;
    }

    public function index() {
      $data = $this->book->orderBy('name', 'asc')->get();
      return response()->json([
        'status_code' => 200,
        'status' => 'success',
        'data' => BookResource::collection($data)
      ]);
    }

    public function external_books($name) {
      
      $client = new Client(['base_uri' => 'https://www.anapioficeandfire.com/api/']);
      $response = $client->request('GET', 'books');
      $results = json_decode($response->getBody());
      
      $data = [];
      foreach($results as $result) {
        if($result->name === $name) {
          $data[] = array(
            'name' => $result->name,
            'isbn' => $result->isbn,
            'authors' => $result->authors,
            'number_of_pages' => $result->numberOfPages,
            'publisher' => $result->publisher,
            'country' => $result->country,
            'release_date' => $result->released,
          );
        }
      }
  
      return response()->json([
        'status_code' => 200,
        'status' => 'success',
        'data'	=> $data,
      ]);
    }

    public function create($request) {
      $book = $this->book->create([
        'name'  => $request->name,
        'isbn'  => $request->isbn,
        'country' => $request->country,
        'number_of_pages' => $request->number_of_pages,
        'publisher' => $request->publisher,
        'release_date' => $request->release_date,
      ]);
      $author = new Author([
        'name' => $request->authors,
        'book_id' => $book->id,
      ]);
      $book->authors()->save($author);
      return response()->json([
        'status_code' => 201,
        'status' => 'success',
        'data' => ['book' => new BookResource($book)],
      ], 201);

    }

    public function show($searchParam) {
      return $book = $this->book->where('name', 'LIKE', "%{$searchParam}%")
                   ->orWhere('country', 'LIKE', "%{$searchParam}%", $searchParam)
                   ->orWhere('publisher', 'LIKE', "%{$searchParam}%", $searchParam)
                   ->orWhereYear('release_date', '=', $searchParam)->get();
     if($book) {
       return  response()->json([
         'status_code' => 200,
         'status' => 'success',
         'data' => new BookResource($book)
       ]);
     }
   }

    public function update($id, array $request) {
      $book = $this->book->find($id);

      if(array_key_exists('authors', $request)) {
        $author = Author::where('book_id', $book->id)->first();
        $author->update([ 'name' => $request['authors']]);
      }      
      if($book->update($request)) {
        return  response()->json([
          'status_code' => 200,
          'status' => 'The book '.$book->name.' was updated successfully',
          'data' => new BookResource($book)
        ]);
      }
    }

    public function delete($id) {
      $book = $this->book->find($id);
      $book->delete($id);
      return  response()->json([
        'status_code' => 204,
        'status' => 'The book '.$book->name.' was deleted successfully',
        'data' => new BookResource($book)
      ]);
    }
  }